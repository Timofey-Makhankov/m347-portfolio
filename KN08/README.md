# KN04 - Kubernetes III - Microservices

## Datenbank erstellung

![](./Images/Screenshot%202024-03-18%20152028.png)

## Front-End starten

![](./Images/Screenshot%202024-03-18%20152948.png)

![](./Images/Screenshot%202024-03-18%20154019.png)

![](./Images/Screenshot%202024-03-18%20154045.png)

## Account Komponent Starten

![](./Images/Screenshot%202024-03-18%20154834.png)

## Intergration der Microservices testen

![](./Images/Screenshot%202024-03-18%20160158.png)

## SendRecieve und BuySell Implementieren

## Resultat

![](./Images/Screenshot%202024-04-08%20152337.png)

![](./Images/Screenshot%202024-04-08%20152416.png)

![](./Images/Screenshot%202024-04-08%20152506.png)

![](./Images/Screenshot%202024-04-08%20152652.png)

