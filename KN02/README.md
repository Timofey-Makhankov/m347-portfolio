# KN02 Dockerfile

## A) Dockerfile I

```docker
FROM nginx # nimm die docker image from nginx
COPY static-html-directory /var/www/html # kopiere die html ordner vom host auf das html verzeichnis im docker image
EXPOSE 80 # veröffentliche das Port 80 vom image
```

`docker build -t name:tag .`

![](./Images/Screenshot%202024-02-19%20144257.png)

![](./Images/Screenshot%202024-02-19%20144419.png)

![](./Images/Screenshot%202024-02-19%20144710.png)

![](./Images/Screenshot%202024-02-19%20144721.png)

![](./Images/Screenshot%202024-02-19%20144900.png)

## B) Dockerfile II

Befehle:

`docker build -t tmtbz/private:kn02b-db -f db.dockerfile .`

`docker build -t tmtbz/private:kn02b-web -f web.dockerfile .`

`docker run -d -p 3306:3306 --name KN02-db tmtbz/private:kn02b-db`

`docker run -d -p 5600:80 --name KN02-web --link KN02-db tmtbz/private:kn02b-web`

`docker push tmtbz/private:kn02b-web`

![](./Images/Screenshot%202024-02-19%20154333.png)

![](./Images/Screenshot%202024-02-19%20154458.png)

![](./Images/Screenshot%202024-02-19%20154555.png)

![](./Images/Screenshot%202024-02-19%20154801.png)

![](./Images/Screenshot%202024-03-04%20131229.png)