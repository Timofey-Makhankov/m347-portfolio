# KN07: Kubernetes II

## A) Begriffe und Konzepte erlernen

Der unterschied zwischen einem Replica und einem Pod ist, dass ein Replica ist zuständig, dass alle Pods in einem Cluster richtig konfiguriert und erstellt sind. Ein Pod ist die kleinste einheit im kubernetes und läuft mehrere Containers. Mehrere Pods können in einem Node sein. Ein Node wäre das Betriebsystem, dass pods läuft. ist aber nicht 1 zu 1.

Das Service ist zuständig für die Kommunikation zwischen den nodes im Cluster. Das deployment ist zuständig für die skalierungen der nodes und pods im cluster.

Ingress ist zuständig für den zugriff ausserhalb des Clusters. Es setzt gewisse Netzwerk Regeln und loadbalancing von Aussen Requests. Das schützt dich vor unerlaubten verbindung von aussen.

ein Statefulset ist ähnlich zu einem deployment, aber der unterschied ist, dass es mit jedem Pod teilweise verbunden ist. dass heisst, wenn es ein pod erstellt, wird es von einem beispiel gebaut, aber nicht alle gleich gebaut. damit kann man für gewisse pods verschiedene speicherplatz geben. Ein Beispiel könnte für einen Webserver, dass die index.html muss presistänt gespeichert werden.

## B) Demo Projekt

![](./Images/Screenshot%202024-03-18%20132618.png)

![](./Images/Screenshot%202024-03-18%20132624.png)

![](./Images/Screenshot%202024-03-18%20132633.png)

![](./Images/Screenshot%202024-03-18%20132639.png)

![](./Images/Screenshot%202024-03-18%20135000.png)

Der Unterschied ist, dass wir das deplayoment und service im gleichem Datei erstellt haben. Es wäre auch möglich, in seperaten Dateien es zu speichern.

Der Wert in `mongo-config.yaml` ist korrekt, da der Wert zeigt an das Service name resp. auf das MongoDB. ähnlich wie im docker compose

### Master Node

![](./Images/Screenshot%202024-03-18%20135935.png)

### Node 1

![](./Images/Screenshot%202024-03-18%20135942.png)

### mongoDB service

![](./Images/Screenshot%202024-03-18%20140245.png)

Es zeigt die info der MongoDB service

### Master

![](./Images/Screenshot%202024-03-18%20135000.png)

### Node 1

![](./Images/Screenshot%202024-03-18%20140359.png)

musste nur das IP verändern

Das verbinden zur Datenbank würde nicht gehen, da wir das Port vom AWS nicht eröffnet hat. Das andere Problem wäre, dass wir den Port vom Kubernetes nicht eröffentlichen. mann musste in der db.yml das port eröffnen und im AWS eröffnen, damit wir verbinden könnten

![](./Images/Screenshot%202024-03-18%20142033.png)

Man kann sehen, dass es drei Endpoints hat, anstelle nur eins

### Node 2 IP Adresse

![](./Images/Screenshot%202024-03-18%20142241.png)