# KN05: Arbeit mit Speicher

## A) Bind mounts

`docker run --name kn05a -p 7400:80 -v $PWD/windows/:/usr/share/nginx/html:rw -d nginx:latest`

> side note: -m oder --mount ist ähnlich zu -v oder --volumes, aber der unterschied ist, wie es den Ordner aufnimmt. Beim Mount wird es dir kein Ordner erstellen, wenn es denn nicht existiert. Beil Volumes wird es dir ein Ordner erstellen, wenn kein Ordner existiert.

![](./Images/Screenshot%202024-03-04%20143210.png)

## B) Volumes

`docker volume create kn04-volume`

`docker run --name kn05b-1 -p 7200:80 -v kn04-volume:/usr/share/nginx/html:rw -d nginx:latest`

`docker run --name kn05b-2 -p 7300:80 -v kn04-volume:/usr/share/nginx/html:rw -d nginx:latest`

![](./Images/Screenshot%202024-03-04%20144114.png)

![](./Images/Screenshot%202024-03-04%20144251.png)

## C) Speicher mit docker compose

![](./Images/Screenshot%202024-03-04%20155334.png)

![](./Images/Screenshot%202024-03-04%20155344.png)
