# KN03 - Netzwerk, Sicherheit

## A) Eigenes Netzwerk

```bash
docker network create -d bridge tbz 
```

```bash
docker run --network=tbz -d --name=busybox-1 busybox
```

![](./Images/Screenshot%202024-02-26%20135419.png)

> Side note: ich konnte nicht auf 172.18.0.0/16 setzen, da ein anderes Netzwerk es schon benutzt hat.

`docker inspect busybox-n`

`docker inspect tbz`

`docker inspect bridge`

TBZ netzwerk:

-  "Subnet": "172.21.0.0/16"
-  "Gateway": "172.21.0.1"

Busybox-1: 172.17.0.2

Busybox-2: 172.17.0.3

Busybox-3: 172.21.0.2

Busybox-4: 172.21.0.3

Busybox-1 Default Gateway: 172.17.0.1

Busybox-4 Default Gateway: 172.21.0.1

![](./Images/Screenshot%202024-02-26%20134449.png)

![](./Images/Screenshot%202024-02-26%20134605.png)

Der Unterschied ist, dass beim default bridge, dass sie teilweise verbunden sind, aber nur durch das Ip ist, das sich in der Zeit verändern kann. Wenn ich aber mein eigenes Netzwerk erstelle. kann ich auch dass container namen benutzen, dass sich nicht in der Zeit verändert.

In KN02 befanden sich die Container im gleichen Gateway, aber sie konnten nicht durch den container name kommunizieren. mit dem Link, verbinde ich den neme mit der Ip-Adresse uns so konnte der Container kommunizieren.
