# KN01: Docker Grundlagen

## A) Installation

![](./Images/Screenshot%202024-02-19%20132343.png)

![](./Images/Screenshot%202024-02-19%20132458.png)

## B) Docker Command Line Interface

### Aufgabe 1

`docker -v`

![](./Images/Screenshot%202024-02-19%20132708.png)

### Aufgabe 2

`docker search ubuntu` & `docker search nginx`

![](./Images/Screenshot%202024-02-19%20133002.png)

![](./Images/Screenshot%202024-02-19%20132950.png)

### Aufgabe 3

> run soll angeben, dass wir ein container starten möchten

> -d besagt, dass das container im hintergrund laufen sollte

> -p 80:80 besagt, setzte den host port 80 (das erste 80) auf port 80 im container (zweite zahl nach semicolon)

> docker/getting-started ist das image, dass wir laufen möchten

### Aufgabe 4

`docker pull nginx`

![](./Images/Screenshot%202024-02-19%20133726.png)

`docker create -p 8081:80 nginx:latest`

![](./Images/Screenshot%202024-02-19%20133912.png)

`docker start <CONTAINER ID>`

![](./Images/Screenshot%202024-02-19%20134034.png)

#### webpage

![](./Images/Screenshot%202024-02-19%20134129.png)

### Aufgabe 5

`docker run -d ubuntu:latest`

![](./Images/Screenshot%202024-02-19%20134323.png)

Docker konnte das Image nicht finden. Er musste von Docker hub herunterladen. Nach der herunterladung, konnte docker ein container erstellen und starten. am ende hat er das Id vom container zurückgegeben.

`docker run -it ubuntu:latest`

![](./Images/Screenshot%202024-02-19%20134627.png)

Docker hatte schon das image und konnte ein neues ubuntu container erstellen. Nachdem es erstellt hat, hat er sich mit dem container  auf einem tty auf shell verbunden.

### Aufgabe 6

![](./Images/Screenshot%202024-02-19%20135109.png)

### Aufgabe 7

`docker ps` & `docker ps -a`

![](./Images/Screenshot%202024-02-19%20135233.png)

### Aufgabe 8

`docker stop <CONTAINER NAME>`

![](./Images/Screenshot%202024-02-19%20135408.png)

### Aufgabe 9

`docker rm <CONTAINER ID>`

![](./Images/Screenshot%202024-02-19%20135559.png)

### Aufgabe 10

`docker rmi <IMAGE ID>`

![](./Images/Screenshot%202024-02-19%20135909.png)

## C) Registry und Repository

![](./Images/Screenshot%202024-02-19%20140618.png)

## D) Privates Repository

`docker tag nginx:latest tmtbz/private:nginx`

mit Tags können wir extra details an einem image geben. Es kann auch als differenzierung von Images helfen mit gleichem package name. es wird meistens für versionen von images bezeichtnet. Zum Beispiel ubuntu:latest oder ubuntu:18.04

`docker push Benutzername/reponame:nginx`

![](./Images/Screenshot%202024-02-19%20141950.png)

Ich Pushe mein erstelltes image auf meinem Repo. Ich musste zuerst aber mich im docekr hub einloggen.

![](./Images/Screenshot%202024-02-19%20142423.png)

![](./Images/Screenshot%202024-02-19%20142532.png)
