# KN04 - Docker Compose

## A) Docker Compose: Lokal

### Teil A) Verwendung von Original Images

![](./Images/Screenshot%202024-02-26%20143905.png)

![](./Images/Screenshot%202024-02-26%20152729.png)

![](./Images/Screenshot%202024-03-04%20135819.png)

![](./Images/Screenshot%202024-02-26%20153657.png)

Docker compose up baut / (neu)erstellt, startet und verbindet alle containers, die im docker compose datei definiert werden.

### Teil B) Verwendung Ihrer eigenen Images 

![](./Images/Screenshot%202024-02-26%20160212.png)

![](./Images/Screenshot%202024-02-26%20160233.png)

Der Fehler kommt vor, da wir die werte für db login hard codiert haben, können wir nicht einfach verbinden. Wir könnten im Docker Compose eine environment und im db.php die werte von systemvariable benutzten. da ist es viel einfacher, einstellungen setzen

## B) Docker Compose: Cloud

![](./Images/Screenshot%202024-03-11%20133811.png)

![](./Images/Screenshot%202024-03-11%20133820.png)

![](./Images/Screenshot%202024-03-11%20133830.png)

![](./Images/Screenshot%202024-03-11%20133837.png)

![](./Images/Screenshot%202024-03-11%20134006.png)
