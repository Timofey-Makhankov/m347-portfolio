# KN06: Kubernetes I

## A) Installation

![](./Images/Screenshot%202024-03-11%20145055.png)

## B) Verständnis für Cluster

### node 1

![](./Images/Screenshot%202024-03-11%20145324.png)

### node 2

![](./Images/Screenshot%202024-03-11%20145342.png)

Es zeigt dir, alle verbundete nodes von kubestl

![](./Images/Screenshot%202024-03-11%20145653.png)

Es bechreibt den status von kubernetes und zeigt, wer alles verbunden ist und was es für Rolle es nimmt. Es zeigt auch, was für addon wurden dazu gegeben.

High Availability heisst, dass eine instance im kubernetes soll weiterlaufen, auch wenn einer der Instanzen runter gegangen ist.

![](./Images/Screenshot%202024-03-11%20150201.png)

![](./Images/Screenshot%202024-03-11%20150248.png)

![](./Images/Screenshot%202024-03-11%20150254.png)

![](./Images/Screenshot%202024-03-11%20150338.png)

mit `microk8s leave` kannst du ein node von kubernetes entfernen. Mit `microk8s remove-node {IP}` kann man ein Node von anderen kubernetes node entfernen.

![](./Images/Screenshot%202024-03-11%20150706.png)

![](./Images/Screenshot%202024-03-11%20150904.png)

Worker nodes werden nicht zu den HA (High Availability) gruppe zugehört. Es wird benutzt, wenn diese node auf low-end Geräte betrieben werden

Der Worker node wird nicht im status angezeigt.

![](./Images/Screenshot%202024-03-11%20151721.png)

Da ein Workernode nicht auf die "control plane" arbeiten, können sie den Status nich aufrufen. Da muss man vom Master aufrufen.

mit microk8s bearbeitet man einzelne node in einem Cluster. Mit kubectl bearbeitet man das ganze Kluster, dass mehrere nodes enthaltet. Da muss man `microk8s kubectl` anschrieben, damit man das Cluster bearbeiten kann.
